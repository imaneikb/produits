FROM openjdk:8
EXPOSE 9001
ADD target/mproduits-0.0.1-SNAPSHOT.jar mproduits.jar
ENTRYPOINT ["java","-jar","/mproduits.jar"]
